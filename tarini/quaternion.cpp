#include "quaternion.h"
#include "matrix3.h"
#include "axis_angle.h"
#include "euler.h"
#include <math.h>

Scalar sign(Scalar x) {
	return (x >= 0) ? +1 : -1;
}

Quaternion Quaternion::from(Euler e) {

	Scalar cosX = cos(e.x / 2);
	Scalar sinX = sin(e.x / 2);
	Scalar cosY = cos(e.y / 2);
	Scalar sinY = sin(e.y / 2);
	Scalar cosZ = cos(e.z / 2);
	Scalar sinZ = sin(e.z / 2);

	Scalar quat_x = sinX * cosY * cosZ - cosX * sinY * sinZ;
	Scalar quat_y = cosX * sinY * cosZ + sinX * cosY * sinZ;
	Scalar quat_z = cosX * cosY * sinZ - sinX * sinY * cosZ;
	Scalar quat_w = cosX * cosY * cosZ + sinX * sinY * sinZ;

	return Quaternion(quat_x, quat_y, quat_z, quat_w);
}

Quaternion Quaternion::from(AxisAngle e) {

	Scalar s = sin(e.angle / 2);

	Scalar x = e.axis.x * s;
	Scalar y = e.axis.y * s;
	Scalar z = e.axis.z * s;
	Scalar w = cos(e.angle / 2);

	return Quaternion(x, y, z, w);
}

Quaternion Quaternion::from(Matrix3 m) {
	/*Scalar w = sqrt(fmax(0, 1 + m.m00 + m.m11 + m.m22)) / 2;
	Scalar x = _copysign(w, m.m21 - m.m12);
	Scalar y = _copysign(w, m.m02 - m.m20);
	Scalar z = _copysign(w, m.m10 - m.m01);
	return Quaternion(x, y, z, w);*/

	//return from(AxisAngle::from(m));

	Scalar r11 = m.m00;
	Scalar r12 = m.m01;
	Scalar r13 = m.m02;
	Scalar r21 = m.m10;
	Scalar r22 = m.m11;
	Scalar r23 = m.m12;
	Scalar r31 = m.m20;
	Scalar r32 = m.m21;
	Scalar r33 = m.m22;
	Scalar q0 = (r11 + r22 + r33 + 1) / 4;
	Scalar q1 = (r11 - r22 - r33 + 1) / 4;
	Scalar q2 = (-r11 + r22 - r33 + 1) / 4;
	Scalar q3 = (-r11 - r22 + r33 + 1) / 4;
	if (q0 < 0) {
		q0 = 0;
	}
	if (q1 < 0) {
		q1 = 0;
	}
	if (q2 < 0) {
		q2 = 0;
	}
	if (q3 < 0) {
		q3 = 0;
	}
	q0 = sqrt(q0);
	q1 = sqrt(q1);
	q2 = sqrt(q2);
	q3 = sqrt(q3);
	if (q0 >= q1 && q0 >= q2 && q0 >= q3) {
		q0 *= +1;
		q1 *= sign(r32 - r23);
		q2 *= sign(r13 - r31);
		q3 *= sign(r21 - r12);
	}
	else if (q1 >= q0 && q1 >= q2 && q1 >= q3) {
		q0 *= sign(r32 - r23);
		q1 *= +1;
		q2 *= sign(r21 + r12);
		q3 *= sign(r13 + r31);
	}
	else if (q2 >= q0 && q2 >= q1 && q2 >= q3) {
		q0 *= sign(r13 - r31);
		q1 *= sign(r21 + r12);
		q2 *= +1;
		q3 *= sign(r32 + r23);
	}
	else if (q3 >= q0 && q3 >= q1 && q3 >= q2) {
		q0 *= sign(r21 - r12);
		q1 *= sign(r31 + r13);
		q2 *= sign(r32 + r23);
		q3 *= +1;
	}
	else {
		// ERROR
	}
	Scalar r = sqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
	q0 /= r;
	q1 /= r;
	q2 /= r;
	q3 /= r;

	return Quaternion(q0, q1, q2, q3);
}

Quaternion Quaternion::rotationX(Scalar angleDeg) {
	return from(Euler(LabUtils::degreeToRadian(angleDeg), 0, 0));
}
Quaternion Quaternion::rotationY(Scalar angleDeg) {
	return from(Euler(0, LabUtils::degreeToRadian(angleDeg), 0));
}
Quaternion Quaternion::rotationZ(Scalar angleDeg) {
	return from(Euler(0, 0, LabUtils::degreeToRadian(angleDeg)));
}

Quaternion Quaternion::toFrom(Versor3 to, Versor3 from) {
	//Vector3 crossP = cross(from, to);
	//if (crossP.isEqual(Vector3(0, 0, 0))) {
	AxisAngle axisAngleRot = AxisAngle::toFrom(to, from);
	return Quaternion::from(axisAngleRot);
	//}
	//else {
	//	return Quaternion(crossP.x, crossP.y, crossP.z, 1 + dot(from, to)).normalized();
	//}
}

Quaternion Quaternion::lookAt(Point3 eye, Point3 target, Versor3 up) {
	/*Matrix3 mRot = Matrix3::lookAt(eye, target, up);
	return from(mRot);*/

	Versor3 arrow = normalize(target - eye);
	Quaternion rot1 = Quaternion::toFrom(arrow, Versor3::forward());
	Versor3 localUp = rot1.axisY();
	Quaternion rot2 = Quaternion::toFrom(up, localUp);
	Quaternion quatRes = rot2 * rot1;
	return quatRes.normalized();
}
