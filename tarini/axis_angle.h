#pragma once

#include <math.h>
#include "vector3.h"
#include "point3.h"
#include "versor3.h"

/* AxisAngle class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all expected methods    */

class Matrix3;
class Quaternion;
class Euler;

class AxisAngle {
public:

	// FIELDS
	Versor3 axis;
	Scalar angle;

	// CONSTRUCTORS
	AxisAngle() :axis(Versor3::up()), angle(0) {}
	AxisAngle(Versor3 d, Scalar k) :axis(d), angle(k) {}
	AxisAngle(const Point3& p) : axis(Versor3(p.x, p.y, p.z)), angle(0) {}

	// APPLY
	Vector3 apply(Vector3  v) const {
		// Rodrigues' rotation formula
		return (cos(angle) * v) + (sin(angle) * (cross(axis.asVector(), v))) + ((1 - cos(angle)) * (dot(axis, v)) * axis);
	}
	// Rotations can be applied to versors or vectors just as well
	Versor3 apply(Versor3 dir) const {
		return apply(dir.asVector()).asVersor();
	}
	Point3 apply(Point3 p) const {
		return apply(p.asVector()).asPoint();
	}
	// syntactic sugar: "R( p )" as a synomim of "R.apply( p )"
	Versor3 operator() (Versor3 p) { return apply(p); }
	Point3  operator() (Point3  p) { return apply(p); }
	Vector3 operator() (Vector3 p) { return apply(p); }

	// AXISES
	Versor3 axisX() const;  // TODO A-Ax a
	Versor3 axisY() const;  // TODO A-Ax b
	Versor3 axisZ() const;  // TODO A-Ax c

	// COMBINE
	AxisAngle operator * (AxisAngle r) const {
		// todo
		return AxisAngle();
	}

	// INVERSE
	AxisAngle inverse() const {
		return AxisAngle(axis, -angle);
	}
	void invert() {
		AxisAngle invertedRot = this->inverse();
		axis = invertedRot.axis;
		angle = invertedRot.angle;
	}

	// LOOK AT
	// returns a rotation to look toward target, if you are in eye, and the up-vector is up
	static AxisAngle lookAt(Point3 eye, Point3 target, Versor3 up = Versor3::up());

	// TO FROM
	// returns a rotation
	static AxisAngle toFrom(Versor3 to, Versor3 from) {
		Scalar angle = clockwiseAngleBetween(from, to);
		return AxisAngle(normalize(findAnyPerpendicolarVector(from, to)), angle);
	}
	static AxisAngle toFrom(Vector3 to, Vector3 from) {
		return toFrom(normalize(to), normalize(from));
	}

	// CONVERSIONS
	// conversions to this representation
	static AxisAngle from(Matrix3 m);
	static AxisAngle from(Euler e);     // TODO E2A
	static AxisAngle from(Quaternion q);// TODO Q2A

	// IS ROT
	// does this AxisAngle encode a rotation?
	bool isRot() const {
		return true;
	}

	// IS POINT
	// does this AxisAngle encode a point?
	bool isPoint() const {
		return false;
	}

	// ROTATIONS
	// return a rotation around an axis
	static AxisAngle rotationX(Scalar angleDeg) {
		return AxisAngle(Versor3::right(), angleDeg);
	}
	static AxisAngle rotationY(Scalar angleDeg) {
		return AxisAngle(Versor3::up(), angleDeg);
	}
	static AxisAngle rotationZ(Scalar angleDeg) {
		return AxisAngle(Versor3::forward(), angleDeg);
	}

	// PRINT
	void printf() const {
		std::cout << "x:" << axis.x << ", y:" << axis.y << ", z:" << axis.z << ", angle:" << angle << std::endl;
	}

	// LERP
	// interpolation or roations
	static AxisAngle lerp(const AxisAngle& a, const AxisAngle& b, Scalar t);

};
