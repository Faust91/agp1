#include "vector3.h"
#include "point3.h"

bool Vector3::isEqual(const Vector3& other) {
	Scalar nrm = squaredNorm(this->asPoint() - other.asPoint());
	return LabUtils::combinedToleranceCompare(nrm, 0);
}

Vector3 Vector3::lerp(const Vector3& a, const Vector3& b, Scalar t) {
	return ((1 - t) * a) + (t * b);
}