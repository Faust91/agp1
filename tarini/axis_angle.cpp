#include "axis_angle.h"
#include "matrix3.h"
#include "quaternion.h"
#include "euler.h"
#include <math.h>

AxisAngle AxisAngle::from(Matrix3 m) {
	Vector3 res = Vector3((m.axisY().z - m.axisZ().y), (m.axisZ().x - m.axisX().z), (m.axisY().x - m.axisX().y));
	Scalar angle = acos((m.axisX().x + m.axisY().y + m.axisZ().z - 1.0) / 2.0);
	Versor3 axis = normalize(res);
	return AxisAngle(axis, angle);
}

AxisAngle AxisAngle::from(Quaternion q) {
	q = q.normalized();
	Scalar angle = 2 * acos(q.w);
	Scalar x, y, z;
	Scalar s = sqrt(1 - q.w * q.w);
	x = q.x;
	y = q.y;
	z = q.z;
	Versor3 axis = normalize(Vector3(x, y, z));
	return AxisAngle(axis, angle);
}

AxisAngle AxisAngle::from(Euler e) {
	return from(Quaternion::from(e));
}

AxisAngle AxisAngle::lookAt(Point3 eye, Point3 target, Versor3 up) {
	return from(Matrix3::lookAt(eye, target, up));
}

Versor3 AxisAngle::axisX() const {
	return Quaternion::from(*this).axisX();
}
Versor3 AxisAngle::axisY() const {
	return Quaternion::from(*this).axisY();
}
Versor3 AxisAngle::axisZ() const {
	return Quaternion::from(*this).axisZ();
}

AxisAngle AxisAngle::lerp(const AxisAngle& a, const AxisAngle& b, Scalar t) {
	return from(Quaternion::lerp(Quaternion::from(a), Quaternion::from(b), t));
}