#include "versor3.h"
#include "vector3.h"
#include "point3.h"

bool Versor3::isEqual(const Versor3& b) {
	return LabUtils::combinedToleranceCompare(squaredNorm(this->asVector().asPoint() - b.asVector().asPoint()), 0);
}