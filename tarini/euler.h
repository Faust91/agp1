#pragma once

#include <math.h>
#include "vector3.h"
#include "point3.h"
#include "versor3.h"

/* Euler class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all the expected methods    */

class Quaternion;
class AxisAngle;
class Matrix3;

class Euler {
public:

	// FIELDS
	Scalar x, y, z;

	// CONSTRUCTORS
	Euler() :x(0), y(0), z(0) {}
	Euler(Scalar _x, Scalar _y, Scalar _z) :x(_x), y(_y), z(_z) {}

	// APPLY
	Vector3 apply(Vector3  v) const;
	// Rotations can be applied to versors or vectors just as well
	Versor3 apply(Versor3 dir) const {
		return apply(dir.asVector()).asVersor();
	}
	Point3 apply(Point3 p) const {
		return apply(p.asVector()).asPoint();
	}
	// syntactic sugar: "R( p )" as a synomim of "R.apply( p )"
	Versor3 operator() (Versor3 p) { return apply(p); }
	Point3  operator() (Point3  p) { return apply(p); }
	Vector3 operator() (Vector3 p) { return apply(p); }

	// AXISES
	Versor3 axisX() const;  // TODO E-Ax a
	Versor3 axisY() const;  // TODO E-Ax b
	Versor3 axisZ() const;  // TODO E-Ax c

	// COMBINE
	Euler operator * (Euler b) const {
		return Euler();
	}

	// INVERSE
	Euler inverse() const;
	void invert();

	// LOOK AT
	// returns a rotation to look toward target, if you are in eye, and the up-vector is up
	static Euler lookAt(Point3 eye, Point3 target, Versor3 up = Versor3::up());

	// TO FROM
	// returns a rotation
	static Euler toFrom(Versor3 to, Versor3 from);
	static Euler toFrom(Vector3 to, Vector3 from) {
		return toFrom(normalize(to), normalize(from));
	}

	// CONVERSIONS
	// conversions to this representation
	static Euler from(Quaternion m);
	static Euler from(Matrix3 e);     // TODO E2M
	static Euler from(AxisAngle e); // TODO E2M

	// IS ROT
	// does this Euler encode a rotation?
	bool isRot() const {
		return true;
	}

	// IS POINT
	// does this Euler encode a point?
	bool isPoint() const {
		return false;
	}

	// ROTATIONS
	// return a rotation around an axis
	static Euler rotationX(Scalar angleDeg);   // TODO E-Rx
	static Euler rotationY(Scalar angleDeg);   // TODO E-Rx
	static Euler rotationZ(Scalar angleDeg);   // TODO E-Rx

	// PRINT
	void printf() const {
		std::cout << "x:" << x << ", y:" << y << ", z:" << z << std::endl;
	}

};

// LERP
// interpolation or roations
inline Euler directLerp(const Euler& a, const Euler& b, Scalar t) {
	// TODO E-directLerp: how to interpolate Eulers
	return Euler();
}
inline Euler lerp(const Euler& a, const Euler& b, Scalar t) {
	// TODO E-smartLerp: how to interpolate Eulers
	return Euler();
}
