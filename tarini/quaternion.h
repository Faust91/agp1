#pragma once

#include <iostream>
#include <math.h>
#include "vector3.h"
#include "point3.h"
#include "versor3.h"
#include "lab_utils.h"

/* Quaternion class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all expected methods    */

class Matrix3;
class AxisAngle;
class Euler;

class Quaternion {
public:

	// FIELDS
	Scalar x, y, z, w;

	// CONSTRUCTORS
	Quaternion(Scalar _x, Scalar _y, Scalar _z, Scalar _w) :x(_x), y(_y), z(_z), w(_w) {}
	Quaternion() :x(0), y(0), z(0), w(1) {}
	Quaternion(const Point3& p) :x(p.x), y(p.y), z(p.z), w(0) {}
	Quaternion(const Vector3& p) :x(p.x), y(p.y), z(p.z), w(0) {}
	Quaternion(const Versor3& p) :x(p.x), y(p.y), z(p.z), w(0) {}

	// APPLY
	Vector3 apply(Vector3  v) const {
		Quaternion vAsQuat = Quaternion(v);
		Quaternion vi = ((*this) * vAsQuat) * this->conjugated();
		//Quaternion vi = (this->conjugated() * vAsQuat) * (*this);
		return Vector3(vi.x, vi.y, vi.z);
	}
	Versor3 apply(Versor3 dir) const {
		return apply(dir.asVector()).asVersor();
	}
	Point3 apply(Point3 p) const {
		return apply(p.asVector()).asPoint();
	}
	// SYNTACTIC SUGAR (SAME OF APPLY)
	Versor3 operator() (Versor3 p) { return apply(p); }
	Point3  operator() (Point3  p) { return apply(p); }
	Vector3 operator() (Vector3 p) { return apply(p); }

	// AXISES
	// utilizzando i coniugati dava un risultato diverso rispetto a matrix3
	// cos� ha comunque senso perch� sto ruotando gli assi mondo
	Versor3 axisX() const {
		return this->apply(Versor3(1, 0, 0));
	}
	Versor3 axisY() const {
		return this->apply(Versor3(0, 1, 0));
	}
	Versor3 axisZ() const {
		return this->apply(Versor3(0, 0, 1));
	}
	// QUICK VERSIONS (calcoli su foglio di carta - primo step moltiplicato per q)
	Versor3 quickAxisX() const {
		Quaternion q_con = this->conjugated();
		Quaternion step1Q = Quaternion(q_con.w, q_con.z, -q_con.y, -q_con.x);
		Quaternion step2Q = step1Q * (*this);
		return normalize(Vector3(step2Q.x, step2Q.y, step2Q.z));
	}
	Versor3 quickAxisY() const {
		Quaternion q_con = this->conjugated();
		Quaternion step1Q = Quaternion(-q_con.z, q_con.w, q_con.x, -q_con.y);
		Quaternion step2Q = step1Q * (*this);
		return normalize(Vector3(step2Q.x, step2Q.y, step2Q.z));
	}
	Versor3 quickAxisZ() const {
		Quaternion q_con = this->conjugated();
		Quaternion step1Q = Quaternion(q_con.y, -q_con.x, q_con.w, -q_con.z);
		Quaternion step2Q = step1Q * (*this);
		return normalize(Vector3(step2Q.x, step2Q.y, step2Q.z));
	}

	// COMBINE
	Quaternion operator * (Quaternion r) const {
		Vector3 wVec = Vector3(x, y, z);
		Vector3 vVec = Vector3(r.x, r.y, r.z);
		Scalar h = w;
		Scalar d = r.w;
		Vector3 imm = (wVec * d) + (vVec * h) + cross(wVec, vVec);
		Scalar rea = (h * d) - dot(wVec, vVec);
		return Quaternion(imm.x, imm.y, imm.z, rea);
	}

	// INVERSE
	Quaternion inverse() const {
		return conjugated() / squaredNorm();
	}
	void invert() {
		*this = inverse();
	}

	// CONJUGATED
	Quaternion conjugated() const {
		return Quaternion(-x, -y, -z, w);
	}
	void conjugate() {
		x = -x;
		y = -y;
		z = -z;
	}

	// LOOK AT
	// returns a rotation to look toward target, if you are in eye, and the up-vector is up
	static Quaternion lookAt(Point3 eye, Point3 target, Versor3 up = Versor3::up());

	// TO FROM
	// returns a rotation
	// implemented in quaternion.cpp
	static Quaternion toFrom(Versor3 to, Versor3 from);
	static Quaternion toFrom(Vector3 to, Vector3 from) {
		return toFrom(normalize(to), normalize(from));
	}

	// CONVERSIONS
	// conversions to this representation
	static Quaternion from(Matrix3 m);   // TODO M2Q
	static Quaternion from(Euler e); // implemented in quaternion.cpp
	static Quaternion from(AxisAngle e); // implemented in quaternion.cpp

	// IS ROT
	// does this quaternion encode a rotation?
	bool isRot() const {
		return squaredNorm() == 1;
	}

	// IS POINT
	// does this quaternion encode a poont?
	bool isPoint() const {
		return w == 0;
	}

	// NORMS
	Scalar squaredNorm() const {
		return x * x + y * y + z * z + w * w;
	}
	Scalar norm() const {
		return sqrt(squaredNorm());
	}

	// NORMALIZE
	Quaternion normalized() const {
		return *this / norm();
	}

	// OPS WITH SCALAR
	Quaternion operator*(Scalar k) const {
		return Quaternion(x * k, y * k, z * k, w * k);
	}
	Quaternion operator/(Scalar k) const {
		return Quaternion(x / k, y / k, z / k, w / k);
	}

	// ROTATIONS
	static Quaternion rotationX(Scalar angleDeg); // implemented in quaternion.cpp
	static Quaternion rotationY(Scalar angleDeg); // implemented in quaternion.cpp
	static Quaternion rotationZ(Scalar angleDeg); // implemented in quaternion.cpp

	// PRINT
	void printf() const {
		std::cout << "x:" << x << ", y:" << y << ", z:" << z << ", w:" << w << std::endl;
	}

	// LERP
// interpolation or roations
	static Quaternion lerp(const Quaternion& a, const Quaternion& b, Scalar t) {
		Quaternion q;
		q.x = (1 - t) * a.x + t * b.x;
		q.y = (1 - t) * a.y + t * b.y;
		q.z = (1 - t) * a.z + t * b.z;
		q.w = (1 - t) * a.w + t * b.w;
		return q.normalized();
	}

};