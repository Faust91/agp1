#include <iostream>
#include <math.h>
#include "transform.h"
#include "lab_utils.h"
#include "euler.h"
#include "axis_angle.h"
#include "quaternion.h"
#include "matrix3.h"

void check(bool condition, std::string descr) {
	std::cout << "Test " << descr << ": "
		<< ((condition) ? "PASSED" : "FAILED")
		<< "\n";

}

void unitTestInverse() {
	Transform t;
	t.translation = Vector3(2, 0, 0);
	t.scale = 3;

	Point3 p0, q, p1;

	q = t.apply(p0);

	Transform ti = t.inverse();
	p1 = ti.apply(q); // VOGLIO RIOTTENERE p

	check(p0.isEqual(p1), "INVERSE");
}


void unitTestCumulate() {
	Transform t1, t2;
	t1.translation = Vector3(2, 0, 0);
	t1.scale = 4;
	t2.translation = Vector3(4, -3, 1);
	t2.scale = 0.4;

	Point3 p(3, 1, 3);
	Point3 q0 = t2.apply(t1.apply(p));
	Point3 q1 = (t2 * t1).apply(p);

	check(q0.isEqual(q1), "CUMULATE");

}

void unitTestRotateQuaternion() {
	Vector3 obj = Vector3(6, 7, 8);
	Euler eul1 = Euler(LabUtils::pi(), 0, 0);
	Quaternion rotation1 = Quaternion::from(eul1);
	Euler eul2 = Euler(0, LabUtils::pi(), 0);
	Quaternion rotation2 = Quaternion::from(eul2);
	Euler eul3 = Euler(0, 0, LabUtils::pi());
	Quaternion rotation3 = Quaternion::from(eul3);
	/*Euler eul4 = Euler(PI / 2, PI / 2, PI / 2);
	Quaternion rotation4 = Quaternion::from(eul4);*/

	Vector3 rotatedObj1 = rotation1.apply(obj);
	//rotatedObj1.printf();
	Vector3 rotatedObj2 = rotation2.apply(obj);
	//rotatedObj2.printf();
	Vector3 rotatedObj3 = rotation3.apply(obj);
	//rotatedObj3.printf();
	//Vector3 rotatedObj4 = rotation4.apply(obj);
	//rotatedObj4.printf();

	Vector3 expectedRes1 = Vector3(6, -7, -8);
	Vector3 expectedRes2 = Vector3(-6, 7, -8);
	Vector3 expectedRes3 = Vector3(-6, -7, 8);
	Vector3 expectedRes4 = Vector3(6, 7, 8);

	check(rotatedObj1.isEqual(expectedRes1) && rotatedObj2.isEqual(expectedRes2) && rotatedObj3.isEqual(expectedRes3), "ROTATE VECTOR WITH QUATERNION");
}

void unitTestVectorAsVersor() {
	Vector3 vect(0, 0, 1);
	Versor3 vers = vect.asVersor();

	check(true, "ASSERTION VECTOR AS VERSOR");
}

void unitTestQuaternionIdentity() {
	Vector3 obj = Vector3(1, 2, 3);
	Euler eul = Euler(0, 0, 0);
	Quaternion rotation = Quaternion::from(eul);
	Quaternion identityQuat = Quaternion();

	Vector3 rotatedObj1 = rotation.apply(obj);
	Vector3 rotatedObj2 = identityQuat.apply(obj);

	check(obj.isEqual(rotatedObj1) && rotatedObj1.isEqual(rotatedObj2), "QUATERNION IDENTITY");
}

void unitTestEulQuatEul() {
	Euler eul = Euler(LabUtils::pi(), LabUtils::pi() / 2, LabUtils::pi() / 4);
	Quaternion quat = Quaternion::from(eul);
	Euler eul2 = Euler::from(quat);

	eul.printf();
	eul2.printf();

	//check(eul.axisX().isEqual(eul2.axisX()) && eul.axisY().isEqual(eul2.axisY()) && eul.axisZ().isEqual(eul2.axisZ()), "EULER QUATERNION EULER");
}

void unitTestQuatAxises() {
	Euler eul = Euler(LabUtils::pi() / 4, 0, 0);
	Quaternion quat = Quaternion::from(eul);

	/*quat.axisX().printf();
	quat.axisY().printf();
	quat.axisZ().printf()*/;
	// TODO check with matrix3
}

void unitTestQuatAxisesQuickVersion() {
	Euler eul = Euler(LabUtils::pi() / 4, 0, 0);
	Quaternion quat = Quaternion::from(eul);

	/*quat.quickAxisX().printf();
	quat.quickAxisY().printf();
	quat.quickAxisZ().printf();*/
	// TODO check with matrix3
}

void unitTestToFromQuaternion() {
	Vector3 vect1 = Vector3(10, 0, 0);
	Vector3 vect2 = Vector3(-10, 0, 0);

	Quaternion res1 = Quaternion::toFrom(vect1, vect1);
	Quaternion res2 = Quaternion::toFrom(vect2, vect1);

	/*vect1.printf();
	res1.printf();
	res2.printf();*/

	Vector3 vect1b = res1.apply(vect1);
	Vector3 vect2b = res2.apply(vect1);

	/*vect1b.printf();
	vect2b.printf();*/

	check(vect1.isEqual(vect1b) && vect2.isEqual(vect2b), "TO-FROM QUATERNION");
}

void unitTestToFromAxisAngle() {
	Vector3 vect1 = Vector3(10, 0, 0);
	Vector3 vect2 = Vector3(-10, 0, 0);

	AxisAngle res1 = AxisAngle::toFrom(vect1, vect1);
	AxisAngle res2 = AxisAngle::toFrom(vect2, vect1);

	/*vect1.printf();
	res1.printf();
	res2.printf();*/

	Vector3 vect1b = res1.apply(vect1);
	Vector3 vect2b = res2.apply(vect1);

	/*vect1b.printf();
	vect2b.printf();*/

	check(vect1.isEqual(vect1b) && vect2.isEqual(vect2b), "TO-FROM AXIS-ANGLE");
}

void unitTestQuaternionInterpolate() {
	Vector3 vect1 = Vector3(0, 0, 50);
	Vector3 vect2 = Vector3(50, 0, 0);
	Versor3 versMid = normalize(Vector3(50, 0, 50));

	Quaternion rot = Quaternion::toFrom(vect2, vect1);

	Quaternion halfRot = Quaternion::lerp(Quaternion(), rot, 0.5);

	Versor3 versRes = normalize(halfRot.apply(vect1));

	check(versMid.isEqual(versRes), "INTERPOLATE QUATERNIONS");
}

void unitTestLookAtMatrix3() {
	Point3 eye = Point3(0, 30, 80);
	Point3 target = Point3(-100, -2, 80);
	// CON I PUNTI QUA SOTTO PASSA I TEST
	/*Point3 eye = Point3(0, 0, 0);
	Point3 target = Point3(-100, 0, 0);*/
	Versor3 up = Versor3::up();
	// mi aspetto una rotazione a sinistra di 90 gradi

	Matrix3 rot = Matrix3::lookAt(eye, target, up);

	Versor3 dir = rot.axisZ();
	Versor3 expectedDir = Versor3::left();

	dir.printf();

	check(expectedDir.isEqual(dir), "LOOK AT MATRIX");
}

void unitTestLookAtQuaternion() {
	Point3 eye = Point3(0, 30, 80);
	Point3 target = Point3(-100, -2, 80);
	// CON I PUNTI QUA SOTTO PASSA I TEST
	/*Point3 eye = Point3(0, 0, 0);
	Point3 target = Point3(-100, 0, 0);*/
	Versor3 up = Versor3::up();
	// mi aspetto una rotazione a sinistra di 90 gradi

	Quaternion rot = Quaternion::lookAt(eye, target, up);

	// FUNZIONAVA QUANDO FACEVO axisY come coniugato.apply(Versor3:up) e poi doppia conversione di rot come Quaternion::from(Matrix3::from(rot))
	// POSSIBILE ERRORE CON GLI ASSI? QUELLA SERIE DI OPERAZIONI FACEVA PASSARE IL TEST!

	Versor3 dir = rot.axisZ();
	Versor3 expectedDir = Versor3::left();

	dir.printf();

	check(expectedDir.isEqual(dir), "LOOK AT QUATERNION");
}


int main()
{
	unitTestInverse();
	unitTestCumulate();
	unitTestRotateQuaternion();
	unitTestVectorAsVersor();
	unitTestQuaternionIdentity();
	//unitTestEulQuatEul();
	/*unitTestQuatAxises();
	unitTestQuatAxisesQuickVersion();*/
	unitTestToFromQuaternion();
	unitTestToFromAxisAngle();
	unitTestQuaternionInterpolate();
	unitTestLookAtMatrix3();
	unitTestLookAtQuaternion();

	return 0;
}
