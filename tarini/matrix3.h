#pragma once

#include <math.h>
#include "vector3.h"
#include "point3.h"
#include "versor3.h"
#include "lab_utils.h"

/* Matrix3 class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all the expected methods    */

class Quaternion;
class AxisAngle;
class Euler;

class Matrix3 {
public:
	// FIELDS
	Scalar m00, m01, m02, m10, m11, m12, m20, m21, m22;

	// GETTERS
	Vector3 getX() const {
		return Vector3(m00, m10, m20);
	}
	Vector3 getY() const {
		return Vector3(m01, m11, m21);
	}
	Vector3 getZ() const {
		return Vector3(m02, m12, m22);
	}
	// SETTERS
	void setX(Vector3 _x) {
		m00 = _x.x;
		m10 = _x.y;
		m20 = _x.z;
	}
	void setY(Vector3 _y) {
		m01 = _y.x;
		m11 = _y.y;
		m21 = _y.z;
	}
	void setZ(Vector3 _z) {
		m02 = _z.x;
		m12 = _z.y;
		m22 = _z.z;
	}

	// CONSTRUCTORS
	Matrix3() :
		m00(1), m01(0), m02(0),
		m10(0), m11(1), m12(0),
		m20(0), m21(0), m22(1) {}
	Matrix3(Scalar _m00, Scalar _m01, Scalar _m02,
		Scalar _m10, Scalar _m11, Scalar _m12,
		Scalar _m20, Scalar _m21, Scalar _m22) :
		m00(_m00), m01(_m01), m02(_m02),
		m10(_m10), m11(_m11), m12(_m12),
		m20(_m20), m21(_m21), m22(_m22) {}
	Matrix3(Vector3 _x, Vector3 _y, Vector3 _z) :
		m00(_x.x), m10(_x.y), m20(_x.z),
		m01(_y.x), m11(_y.y), m21(_y.z),
		m02(_z.x), m12(_z.y), m22(_z.z) {}

	// APPLY
	Vector3 apply(Vector3  v) const {
		Scalar x = getX().x * v.x + getY().x * v.y + getZ().x * v.z;
		Scalar y = getX().y * v.x + getY().y * v.y + getZ().y * v.z;
		Scalar z = getX().z * v.x + getY().z * v.y + getZ().z * v.z;
		return Vector3(x, y, z);
	}
	// Rotations can be applied to versors or vectors just as well
	Versor3 apply(Versor3 dir) const {
		return apply(dir.asVector()).asVersor();
	}
	Point3 apply(Point3 p) const {
		return apply(p.asVector()).asPoint();
	}
	// syntactic sugar: "R( p )" as a synomim of "R.apply( p )"
	Versor3 operator() (Versor3 p) { return apply(p); }
	Point3  operator() (Point3  p) { return apply(p); }
	Vector3 operator() (Vector3 p) { return apply(p); }

	// AXISES
	Versor3 axisX() const {
		return normalize(Vector3(m00, m10, m20));
	}
	Versor3 axisY() const {
		return normalize(Vector3(m01, m11, m21));
	}
	Versor3 axisZ() const {
		return normalize(Vector3(m02, m12, m22));
	}

	// COMBINE
	// combine two rotations (r goes first!)
	Matrix3 operator * (Matrix3 r) const {
		Scalar nm00 = m00 * r.m00 + m01 * r.m10 + m02 * r.m20;
		Scalar nm01 = m00 * r.m01 + m01 * r.m11 + m02 * r.m21;
		Scalar nm02 = m00 * r.m02 + m01 * r.m12 + m02 * r.m22;
		Scalar nm10 = m10 * r.m00 + m11 * r.m10 + m12 * r.m20;
		Scalar nm11 = m10 * r.m01 + m11 * r.m11 + m12 * r.m21;
		Scalar nm12 = m10 * r.m02 + m11 * r.m12 + m12 * r.m22;
		Scalar nm20 = m20 * r.m00 + m21 * r.m10 + m22 * r.m20;
		Scalar nm21 = m20 * r.m01 + m21 * r.m11 + m22 * r.m21;
		Scalar nm22 = m20 * r.m02 + m21 * r.m12 + m22 * r.m22;
		return Matrix3(nm00, nm01, nm02, nm10, nm11, nm12, nm20, nm21, nm22);
	}

	Matrix3 inverse() const {
		return this->transposed();
	}

	void invert() {
		this->transpose();
	}

	// specific methods for matrix...
	Matrix3 transposed() const {
		Matrix3 newMatrix = Matrix3(m00, m01, m02, m10, m11, m12, m20, m21, m22);
		newMatrix.m01 = m10;
		newMatrix.m10 = m01;
		newMatrix.m12 = m21;
		newMatrix.m21 = m12;
		newMatrix.m02 = m20;
		newMatrix.m20 = m02;
		return newMatrix;
	}

	void transpose() {
		Matrix3 tM = this->transposed();
		m00 = tM.m00;
		m01 = tM.m01;
		m02 = tM.m02;
		m10 = tM.m10;
		m11 = tM.m11;
		m12 = tM.m12;
		m20 = tM.m20;
		m21 = tM.m21;
		m22 = tM.m22;
	}

	// returns a rotation to look toward target, if you are in eye, and the up-vector is up
	static Matrix3 lookAt(Point3 eye, Point3 target, Versor3 up = Versor3::up());

	// returns a rotation
	static Matrix3 toFrom(Versor3 to, Versor3 from);
	static Matrix3 toFrom(Vector3 to, Vector3 from) {
		return toFrom(normalize(to), normalize(from));
	}

	// conversions to this representation
	static Matrix3 from(Quaternion m);
	static Matrix3 from(Euler e);
	static Matrix3 from(AxisAngle e);

	Scalar det() const {
		return m00 * LabUtils::det2x2(m11, m12, m21, m22)
			- m01 * LabUtils::det2x2(m10, m12, m20, m22)
			+ m02 * LabUtils::det2x2(m10, m11, m20, m21);
	}

	// does this Matrix3 encode a rotation?
	bool isRot() const {
		bool retVal = LabUtils::combinedToleranceCompare(det(), 1);
		retVal &= cross(getX(), getY()).isEqual(getZ());
		retVal &= cross(getY(), getZ()).isEqual(getX());
		retVal &= cross(getZ(), getX()).isEqual(getY());
		return retVal;
	}

	// return a rotation matrix around an axis
	static Matrix3 rotationX(Scalar angleDeg);
	static Matrix3 rotationY(Scalar angleDeg);
	static Matrix3 rotationZ(Scalar angleDeg);

	// PRINT
	void printf() const {
		std::cout << "|" << m00 << " " << m01 << " " << m02 << "|" << std::endl;
		std::cout << "|" << m10 << " " << m11 << " " << m12 << "|" << std::endl;
		std::cout << "|" << m20 << " " << m21 << " " << m22 << "|" << std::endl;
	}
};


// interpolation of roations
inline Matrix3 directLerp(const Matrix3& a, const Matrix3& b, Scalar t) {
	// TODO M-directLerp: how to interpolate Matrix3s
	return Matrix3();
}

inline Matrix3 lerp(const Matrix3& a, const Matrix3& b, Scalar t) {
	// TODO M-smartLerp: how to interpolate Matrix3s
	return Matrix3();
}




