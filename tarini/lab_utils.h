#pragma once

#include <cmath>
#include <limits>
#include <algorithm>
#include <type_traits>

#define PI 3.14159265359;

typedef double Scalar;

//static const Scalar EPSILON = std::numeric_limits<Scalar>::epsilon();
static const Scalar EPSILON = 0.00000001;
static const Scalar EPSILON2 = EPSILON * EPSILON;

class LabUtils {

public:

	static bool combinedToleranceCompare(Scalar x, Scalar y)
	{
		double maxXYOne = std::max({ 1.0, std::fabs(x) , std::fabs(y) });

		return std::fabs(x - y) <= EPSILON * maxXYOne;
	}

	static Scalar det2x2(Scalar a, Scalar b, Scalar c, Scalar d) {
		return (a * d) - (b * c);
	}

	static Scalar degreeToRadian(Scalar degree) {
		return (degree * (pi() / 180));
	}

	static Scalar pi() {
		return PI;
	}

	static Scalar epsilon() {
		return EPSILON;
	}

};