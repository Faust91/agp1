﻿#pragma once
#include <math.h>
#include <assert.h>
#include "vector3.h"
#include "lab_utils.h"

class Versor3 {
	// constructors
public:
	Scalar x, y, z;

	static Versor3 right() { return Versor3(+1, 0, 0); } // aka EAST
	static Versor3 left() { return Versor3(-1, 0, 0); } // aka WEST
	static Versor3 up() { return Versor3(0, +1, 0); }
	static Versor3 down() { return Versor3(0, -1, 0); }
	static Versor3 forward() { return Versor3(0, 0, +1); } // aka NORTH
	static Versor3 backward() { return Versor3(0, 0, -1); } // aka SOUTH

	Versor3(Scalar _x, Scalar _y, Scalar _z) :x(_x), y(_y), z(_z) { }

	Versor3() :x(0), y(0), z(0) { }

	// access to the coordinates: to write them
	Scalar& operator[] (int i) {
		if (i == 0) return x;
		if (i == 1) return y;
		if (i == 2) return z;
		//assert(0);
		return x;
	}

	// access to the coordinates: to read them
	Scalar operator[] (int i) const {
		if (i == 0) return x;
		if (i == 1) return y;
		if (i == 2) return z;
		//assert(0);
		return x;
	}

	Vector3 operator*(Scalar k) const {
		return Vector3(k * x, k * y, k * z);
	}

	Versor3 operator -() const {
		return Versor3(-x, -y, -z);
	}
	Versor3 operator +() const {
		return Versor3(x, y, z);
	}

	friend Versor3 normalize(Vector3 p);
	friend Versor3 Vector3::asVersor() const;

	Vector3 asVector() const {
		//return *this * 1;
		return Vector3(x, y, z);
	}

	bool isEqual(const Versor3& b);

	void printf() const {
		std::cout << "x:" << x << ", y:" << y << ", z:" << z << std::endl;
	}

};

inline Versor3 normalize(Vector3 p) {
	Scalar n = norm(p);
	if (n > 0) {
		return Versor3(p.x / n, p.y / n, p.z / n);
	}
	else {
		return Versor3();
	}
}

// cosine between a and b
inline Scalar dot(const Versor3& a, const Versor3& b) {
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

// lenght of projection of b along a
inline Scalar dot(const Versor3& a, const Vector3& b) {
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline Vector3 cross(const Versor3& a, const Versor3& b) {
	return Vector3(
		a.y * b.z - a.z * b.y,
		a.z * b.x - a.x * b.z,
		a.x * b.y - a.y * b.x);
}

inline Vector3 findAnyPerpendicolarVector(const Versor3& from, const Versor3& to) {
	Vector3 crossP = cross(from, to);
	// il cross è venuto 0
	if (crossP.isEqual(Vector3(0, 0, 0))) {
		// posso prendere indifferentemente from o to per questi controlli...
		if (!(from.x + from.y < LabUtils::epsilon() && from.z < LabUtils::epsilon())) {
			crossP = Vector3(from.z, from.z, -(from.x + from.y));
		}
		else if (!(from.y + from.z < LabUtils::epsilon() && from.x < LabUtils::epsilon())) {
			crossP = Vector3(-(from.y + from.z), from.x, from.x);
		}
		else {
			crossP = Vector3(from.y, -(from.x + from.z), from.y);
		}
	}
	return crossP;
}


inline Vector3 operator*(Scalar k, const Versor3& a) { return a * k; }

inline Versor3 nlerp(const Versor3& a, const Versor3& b, Scalar t) {
	// TODO nlerp
	return Versor3::up();
}

inline Versor3 slerp(const Versor3& a, const Versor3& b, Scalar t) {
	// TODO slerp
	return Versor3::up();
}

// under my own resposability, I declare this vector to be unitary and therefore a VERSOR
inline Versor3 Vector3::asVersor() const {
	// TODO: a nice assert?
	Versor3 vers = Versor3(x, y, z);
	Versor3 normVect = normalize(*this);
	assert(LabUtils::combinedToleranceCompare(vers.x, normVect.x) && LabUtils::combinedToleranceCompare(vers.y, normVect.y) && LabUtils::combinedToleranceCompare(vers.z, normVect.z));
	return Versor3(x, y, z);
}


inline Scalar clockwiseAngleBetween(Versor3 a, Versor3 b) {
	Scalar dtoP = dot(a, b);
	return acos(dtoP);
}
