#include "euler.h"
#include "quaternion.h"
#include "matrix3.h"
#include "axis_angle.h"
#include "lab_utils.h"

Euler Euler::from(Quaternion q) {
	Euler eul;

	// roll (x-axis rotation)
	double sinX_cosY = 2 * (q.w * q.x + q.y * q.z);
	double cosX_cosY = 1 - 2 * (q.x * q.x + q.y * q.y);
	eul.x = std::atan2(sinX_cosY, cosX_cosY);

	// pitch (y-axis rotation)
	double sinY = 2 * (q.w * q.y - q.z * q.x);
	if (std::abs(sinY) >= 1)
		eul.y = std::copysign(LabUtils::pi() / 2, sinY); // use 90 degrees if out of range
	else
		eul.y = std::asin(sinY);

	// yaw (z-axis rotation)
	double sinY_cosY = 2 * (q.w * q.z + q.x * q.y);
	double cosY_cosY = 1 - 2 * (q.y * q.y + q.z * q.z);
	eul.z = std::atan2(sinY_cosY, cosY_cosY);

	return eul;
}

Vector3 Euler::apply(Vector3  v) const {
	return Quaternion::from(*this).apply(v);

}

Versor3 Euler::axisX() const {
	return Quaternion::from(*this).axisX();
}
Versor3 Euler::axisY() const {
	return Quaternion::from(*this).axisY();
}
Versor3 Euler::axisZ() const {
	return Quaternion::from(*this).axisZ();
}

Euler Euler::inverse() const {
	return from((Quaternion::from(*this)).inverse());
}
void Euler::invert() {
	Euler inv = this->inverse();
	x = inv.x;
	y = inv.y;
	z = inv.z;
}

Euler Euler::lookAt(Point3 eye, Point3 target, Versor3 up) {
	return Euler::from(Quaternion::lookAt(eye, target, up));
}

// TO FROM
// returns a rotation
Euler Euler::toFrom(Versor3 to, Versor3 from) {
	return Euler::from(Quaternion::toFrom(to, from));
}

Euler Euler::from(Matrix3 e) {
	return Euler::from(Quaternion::from(e));
}
Euler Euler::from(AxisAngle e) {
	return Euler::from(Quaternion::from(e));
}

Euler Euler::rotationX(Scalar angleDeg) {
	return Euler::from(Quaternion::rotationX(angleDeg));
}
Euler Euler::rotationY(Scalar angleDeg) {
	return Euler::from(Quaternion::rotationY(angleDeg));
}
Euler Euler::rotationZ(Scalar angleDeg) {
	return Euler::from(Quaternion::rotationZ(angleDeg));
}