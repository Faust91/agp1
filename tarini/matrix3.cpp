#include "matrix3.h"
#include "quaternion.h"
#include "euler.h"
#include "axis_angle.h"
#include "lab_utils.h"

Matrix3 Matrix3::lookAt(Point3 eye, Point3 target, Versor3 up) {
	Versor3 dir = normalize(target - eye);
	Vector3 z = dir.asVector();
	Vector3 x = normalize(findAnyPerpendicolarVector(Versor3::up(), dir)).asVector();
	Vector3 y = normalize(cross(z, x)).asVector();
	return Matrix3(x, y, z);
}

Matrix3 Matrix3::from(Quaternion q)
{
	Scalar m00 = 1.0 - 2.0 * (q.y * q.y) - 2.0 * (q.z * q.z);
	Scalar m10 = 2.0 * (q.x * q.y) - 2.0 * (q.w * q.z);
	Scalar m20 = 2.0 * (q.x * q.z) + 2.0 * (q.w * q.y);

	Versor3 x = normalize(Vector3(m00, m10, m20));

	Scalar m01 = 2.0 * (q.x * q.y) + 2.0 * (q.w * q.z);
	Scalar m11 = 1.0 - 2.0 * (q.x * q.x) - 2.0 * (q.z * q.z);
	Scalar m21 = 2.0 * (q.y * q.z) - 2.0 * (q.w * q.x);

	Versor3 y = normalize(Vector3(m01, m11, m21));

	Scalar m02 = 2.0 * (q.x * q.z) - 2.0 * (q.w * q.y);
	Scalar m12 = 2.0 * (q.y * q.z) + 2.0 * (q.w * q.x);
	Scalar m22 = 1.0 - 2.0 * (q.x * q.x) - 2.0 * (q.y * q.y);

	Versor3 z = normalize(Vector3(m02, m12, m22));

	return Matrix3(x.asVector(), y.asVector(), z.asVector());
}

Matrix3 Matrix3::from(Euler e) {
	return Matrix3::from(Quaternion::from(e));
}
Matrix3 Matrix3::from(AxisAngle e) {
	return Matrix3::from(Quaternion::from(e));
}

Matrix3 Matrix3::rotationX(Scalar angleDeg) {
	Scalar rads = LabUtils::degreeToRadian(angleDeg);
	Matrix3 rot = Matrix3(); // I
	rot.m11 = cos(rads);
	rot.m21 = sin(rads);
	rot.m12 = -sin(rads);
	rot.m22 = cos(rads);
	return rot;
}
Matrix3 Matrix3::rotationY(Scalar angleDeg) {
	Scalar rads = LabUtils::degreeToRadian(angleDeg);
	Matrix3 rot = Matrix3(); // I
	rot.m00 = cos(rads);
	rot.m20 = -sin(rads);
	rot.m02 = sin(rads);
	rot.m22 = cos(rads);
	return rot;
}
Matrix3 Matrix3::rotationZ(Scalar angleDeg) {
	Scalar rads = LabUtils::degreeToRadian(angleDeg);
	Matrix3 rot = Matrix3(); // I
	rot.m00 = cos(rads);
	rot.m01 = -sin(rads);
	rot.m10 = sin(rads);
	rot.m11 = cos(rads);
	return rot;
}

Matrix3 Matrix3::toFrom(Versor3 to, Versor3 from) {
	return Matrix3::from(Quaternion::toFrom(to, from));
}